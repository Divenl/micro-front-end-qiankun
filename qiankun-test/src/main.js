import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import { registerMicroApps } from 'qiankun';

registerMicroApps([
    // {
    //     name: "vue2App",
    //     props: { age: 10 }, //给子应用传数据
    //     entry: "//localhost:3001", //默认会加载这个html,解析里面的js,动态执行（子应用必须支持跨域）里面,是用fetch去请求的数据
    //     container: "#out-main", //挂载到主应用的哪个元素下
    //     activeRule: "/vue2", //当我劫持到路由地址为/vue2时，我就把http://localhost:3000这个应用挂载到#app-main的元素下
    // },
    {
        name: 'vueChildOne',
        entry: '//192.168.2.120:3001',
        // entry: { scripts: ['//http://192.168.2.120:3001/main.js'] },
        container: '#child-one-content',
        activeRule: '/child-one',
    },
    {
        name: 'vueChildTwo',
        entry: '//192.168.2.120:3002',
        // entry: { scripts: ['//http://192.168.2.120:3001/main.js'] },
        container: '#child-two-content',
        activeRule: '/child-two',
    },
    // {
    //     name: 'vueChildTwo',
    //     entry: '//192.168.2.120:9527',
    //     // entry: { scripts: ['//http://192.168.2.120:3001/main.js'] },
    //     container: '#child-two-content',
    //     activeRule: '/child-two',
    // },
]);

// setDefaultMountApp('/child-one')

// 启动 qiankun
// start();

createApp(App).use(ElementPlus).use(router).mount('#app-base')
